import { Card, Button, Spinner } from "flowbite-react";
import { useState } from "react";

const femaleOption = [
  "Nipples",
  "Ears",
  "Neck",
  "Bum",
  "Boobs",
  "Thighs",
  "G-Spot",
  "Vagina",
  "Belly Button",
  "Chest",
  "Stomach",
  "Back",
  "Lips",
];

const MaleOptions = [
  "Nipples",
  "Penis",
  "Balls",
  "Chest",
  "Ears",
  "Neck",
  "Back",
  "Lips",
];

function Games() {
  const [loading, setLoading] = useState(false);
  const [bkl, setBkl] = useState();
  const [loadingMale, setLoadingMale] = useState(false);
  const [male, setMale] = useState();
  const [loadingFemale, setLoadingFemale] = useState(false);
  const [female, setFemale] = useState();

  const getBKL = () => {
    setLoading(true);
    setTimeout(function () {
      setLoading(false);
    }, 1500);
    const num = Math.floor(Math.random() * (20 - 1 + 1) + 1);
    const times = Math.floor(num / 4).toString();
    if (times === "0") {
      setBkl("Click again!!");
    } else if (num % 2 === 0 || num % 7 === 0) {
      setBkl("Bite " + times + " time or more");
    } else if (num % 3 === 0 || num % 5 === 0) {
      setBkl("Kiss " + times + " time or more");
    } else if (num % 13 === 0 || num % 11 === 0) {
      setBkl("Lick " + times + " time or more");
    } else {
      setBkl("Click again!!");
    }
  };

  const getMale = () => {
    setLoadingMale(true);
    setTimeout(function () {
      setLoadingMale(false);
    }, 1500);
    const num = Math.floor(Math.random() * (7 - 1 + 1) + 1);

    setMale(MaleOptions[num]);
  };

  const getFemale = () => {
    setLoadingFemale(true);
    setTimeout(function () {
      setLoadingFemale(false);
    }, 1500);
    const num = Math.floor(Math.random() * (12 - 1 + 1) + 1);
    setFemale(femaleOption[num]);
  };

  return (
    <div className="p-10 lg:p-20 m-auto max-w-screen-xl">
      <div className="grid grid-cols-1 md:grid-cols-3 gap-2">
        <Card className="max-w-sm">
          <h5 className="text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
            Bite/Kiss/Lick
          </h5>
          {loading ? (
            <Spinner color="success" aria-label="Success spinner example" />
          ) : (
            <h6>{bkl}</h6>
          )}
          <Button
            className="mt-2"
            gradientMonochrome="success"
            pill
            onClick={getBKL}
            disabled={loading}
          >
            I want you to
          </Button>
        </Card>
        <Card className="max-w-sm">
          <h5 className="text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
            Ask your Male partner
          </h5>
          {loadingMale ? (
            <Spinner color="info" aria-label="Success spinner example" />
          ) : (
            <h6>{male}</h6>
          )}
          <Button
            className="mt-2"
            gradientMonochrome="teal"
            pill
            onClick={getMale}
            disabled={loadingMale}
          >
            Start
          </Button>
        </Card>
        <Card className="max-w-sm">
          <h5 className="text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
            Ask your Female partner
          </h5>
          {loadingFemale ? (
            <Spinner color="info" aria-label="Success spinner example" />
          ) : (
            <h6>{female}</h6>
          )}
          <Button
            className="mt-2"
            gradientMonochrome="pink"
            pill
            onClick={getFemale}
            disabled={loadingFemale}
          >
            Start
          </Button>
        </Card>
      </div>
    </div>
  );
}

export default Games;
