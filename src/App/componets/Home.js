import { Timeline } from "flowbite-react";
import a2 from "../assets/2.jpg";
import { useState, useEffect } from "react";
import dataJson from "../data.json";

const titles = [
  "I'm wearing the smile you gave me.",
  "The best things in life are better with you.",
  "You're my favorite hello and my hardest goodbye.",
  "The peanut butter to my jelly.",
  "Together is a wonderful place to be.",
  "I can't live without you, and I don't want to try.",
  "My favorite fairytale is our love story.",
  "Home is wherever I'm with you.",
  "Two heads, one heart.",
  "Every day with you is the best day of my life.",
  "I want to be with you until my last page.",
  "P.S. I love you.",
  "You're the apple of my eye.",
  "You're my lobster.",
  "He's the Jim to my Pam.",
  "We have a forever type of love.",
  "Only a fool for you.",
  "I saw that you were perfect, and so I loved you. Then I saw that you were not perfect and I loved you even more.",
  "You know you’re in love when you can’t fall asleep because reality is finally better than your dreams.",
  "Love is that condition in which the happiness of another person is essential to your own.",
  "The best thing to hold onto in life is each other.",
  "I need you like a heart needs a beat.",
  "I am who I am because of you. You are every reason, every hope, and every dream I’ve ever had.",
  "If I had a flower for every time I thought of you... I could walk through my garden forever.",
  "Take my hand, take my whole life too. For I can’t help falling in love with you.",
  "If you live to be a hundred, I want to live to be a hundred minus one day so I never have to live without you.",
  "You’re the closest to heaven, that I’ll ever be.",
  "You are the finest, loveliest, tenderest, and most beautiful person I have ever known and even that is an understatement.",
  "I will never stop trying. Because when you find the one... you never give up.",
  "It’s always better when we’re together.",
  "I love you for all that you are, all that you have been and all that you will be.",
  "To the world you may be one person, but to one person you are the world.",
  "Two are better than one.",
  "I’ve tried so many times to think of a new way to say it, and it’s still I love you.",
  "I love you and that’s the beginning and end of everything.",
  "If I know what love is, it is because of you.",
  "My soul and your soul are forever tangled.",
  "I love you more than I have ever found a way to say to you.",
  "I have found the one whom my soul loves.",
  "Sometimes all you need is a hug from the right person and all your stress will melt away.",
];

function Home() {
  const [showTimeline, setShowTimeline] = useState(false);
  const [selectedYear, setSelectedYear] = useState();

  const [counter, setCounter] = useState(0);

  useEffect(() => {
    setCounter(Math.floor(Math.random() * (40 + 1)));
  }, [counter]);

  const arr = dataJson.map((item) => item.year);
  let uniqueYears = arr.filter((item, index) => arr.indexOf(item) === index);
  let filteredArray = [];
  dataJson.forEach((item) => {
    if (uniqueYears.includes(item.year)) {
      uniqueYears = uniqueYears.slice(1);
      filteredArray.push(item);
    }
  });
  const [data, setData] = useState(filteredArray);

  const filterData = (year) => {
    const arr = dataJson.map((item) => item.year);
    let uniqueYears = arr.filter((item, index) => arr.indexOf(item) === index);
    let filteredArray = [];
    if (year === selectedYear) {
      dataJson.forEach((item) => {
        if (uniqueYears.includes(item.year)) {
          uniqueYears = uniqueYears.slice(1);
          filteredArray.push(item);
        }
      });
      setSelectedYear("");
    } else {
      dataJson.forEach((item) => {
        if (item.year === year) {
          filteredArray.push(item);
        } else if (uniqueYears.includes(item.year)) {
          uniqueYears = uniqueYears.filter((y) => y !== item.year);
          filteredArray.push(item);
        }
      });
      setSelectedYear(year);
    }
    setData(filteredArray);
  };

  if (showTimeline) {
    return (
      <div className="bg-slate-50/50">
        <div className="p-10 lg:p-20 m-auto max-w-screen-xl flex flex-col gap-6">
          <div className="flex flex-col gap-2">
            <h1 className="text-center text-2xl font-medium text-blue-950">
              OUR JOURNEY
            </h1>
            <div className="flex justify-center">
              <div className="h-[4px] bg-blue-950 w-32 rounded-full"></div>
            </div>
          </div>

          <Timeline className="timeline">
            {data.map(({ year, date, text, subText }, index) => (
              <Timeline.Item key={index}>
                <Timeline.Point className="bg-point" />
                <Timeline.Content>
                  <Timeline.Time
                    className="font-medium text-slate-700 cursor-pointer"
                    onClick={() => filterData(year)}
                  >
                    {year}
                  </Timeline.Time>
                  <Timeline.Title className="font-light text-md text-gray-500">
                    {date}
                  </Timeline.Title>
                  <Timeline.Body className="text-slate-900">
                    <p>{text}</p>
                    <p className="text-xs pt-3">{subText}</p>
                  </Timeline.Body>
                </Timeline.Content>
              </Timeline.Item>
            ))}
          </Timeline>
        </div>
      </div>
    );
  }

  return (
    <div className="bg-slate-50/50">
      <div className="m-auto max-w-screen-xl flex justify-center flex flex-col gap-6">
        <div className="flex flex-col gap-2">
          <h2 className="p-4 pb-1 text-center font-medium">
            May be I am not the one you have dream of, I am not the one who will
            be perfect for you but I promise you I am the one who will love you
            forever.
          </h2>
          <p className="p-4 pt-1 text-center">
            <small>“{titles[counter]}”</small>
          </p>
        </div>

        <img src={a2} alt="profile" onClick={() => setShowTimeline(true)}></img>
      </div>
    </div>
  );
}

export default Home;
