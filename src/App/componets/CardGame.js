import "./cardgame.css";
import picture1 from "../assets/games/1-p.png";
import picture2 from "../assets/games/2-p.png";
import picture3 from "../assets/games/3-p.png";
import picture4 from "../assets/games/4-p.png";
import picture5 from "../assets/games/5-p.png";
import picture6 from "../assets/games/6-p.png";
import picture7 from "../assets/games/7-p.png";
import picture8 from "../assets/games/8-p.png";
import picture9 from "../assets/games/9-p.png";
import picture10 from "../assets/games/10-p.png";

function CardGame() {
  const listOfAction = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"];
  const imageList = [
    picture1,
    picture2,
    picture3,
    picture4,
    picture5,
    picture6,
    picture7,
    picture8,
    picture9,
    picture10,
  ];

  return (
    <div className="container mx-auto p-6 md:p-8 lg:p-10 grid grid-cols-2 md:grid-cols-3 lg:grid-cols-5 gap-5">
      {listOfAction.map((item, index) => {
        return (
          <div className="flip-card h-40 rounded-lg overflow-hidden  text-xl bg-transparent">
            <div className="flip-card-inner rounded-lg">
              <div className="flip-card-front flex flex-wrap justify-center items-center bg-slate-50 rounded-lg">
                {index + 1}
              </div>
              <div className="flip-card-back flex flex-wrap justify-center items-center bg-slate-200 rounded-lg">
                <img className="h-full" src={imageList[index]} alt={item} />
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
}

export default CardGame;
