import { useState } from "react";
import Games from "./App/componets/Games";
import Home from "./App/componets/Home";
import CardGame from "./App/componets/CardGame";

function App() {
  const [openGames, setOpenGames] = useState(false);
  const [openGames1, setOpenGames1] = useState(false);
  if (openGames) {
    return <Games />;
  }
  if (openGames1) {
    return <CardGame />;
  }
  return (
    <>
      <Home />
      <span className="text-white" onClick={() => setOpenGames(true)}>
        click me!
      </span>
      <span className="text-white" onClick={() => setOpenGames1(true)}>
        click me!
      </span>
    </>
  );
}

export default App;
